<?php

/** @brief Check the user agent to determine the type of device.
 *
 * This function attempts to determine the type of device in use. It returns a
 * group name, 'mobile', or 'desktop'.
 *
 * This function is based on the function named _mobile_tools_is_mobile_device()
 * found in the mobile_tools module. Only, the switch() was simplified using one
 * array instead.
 *
 * @param[in] $agent  We expect the $_SERVER['HTTP_USER_AGENT'] string or an equivalent.
 *
 * @return The mobile device group, 'mobile', or 'desktop'.
 */
function mobilekey_user_agent_to_device($agent) {
  // the user agent string will be forced to lowercase
  // so all the strings in the following array are lowercase
  static $mobiles = array(
    'ipod' => array('include' => 'ipod'),
    'iphone' => array('include' => 'iphone'),
    'android' => array('include' => 'android'),
    'opera_mini' => array('include' => 'opera mini'),
    'blackberry' => array('include' => 'blackberry'),

    // keep the non-specific case last since it may very well match
    // one of the previous entries otherwise
    'mobile' => array(
      'match' => '/kindle|midp|mmp|mobile|o2|pda|pocket|psp|smartphone|symbian|treo|up\\.browser|up\\.link|vodafone|wap/',
      'start' => array( // first 4 or 3 letters
        // numbers
        '1207','3gso','4thp','501i','502i','503i','504i','505i',
        '506i','6310','6590','770s','802s',
        // A
        'a wa','abac','acer','acoo','acs-','aiko','airn','alav',
        'alca','alco','amoi','anex','anny','anyw','aptu','arch',
        'argo','aste','asus','attw','au-m','audi','aur ','aus ',
        'avan',
        // B
        'bell','benq','beck','bilb','bird','blac','blaz','brew',
        'brvw','bumb','bw-n','bw-u',
        // C
        'c55/','capi','ccwa','cdm-','cell','chtm','cldc','cmd-',
        'comp','cond','craw',
        // D
        'dait','dall','dang','dbte','dc-s','devi','dica','dmob',
        'ds-d','ds12','doco','dopo',
        // E
        'el49','elai','eml2','emul','eric','erk0','esl8','ez40',
        'ez60','ez70','ezos','ezwa','ezze',
        // F
        'fake','fetc','fly-','fly_',
        // G
        'g-mo','g1 u','g560','gene','gf-5','go.w','good','grad',
        'grun',
        // H
        'haie','hcit','hd-m','hd-p','hd-t','hei-','hipt','hita',
        'hp i','hpip','hs-c','htc ','htc-','htca','htcg','htcp',
        'htcs','htct','htc_','http','huaw','hutc',
        // I
        'i-20','i-go','i-ma','i230','iac' ,'iac-','iac/','ibro',
        'idea','ig01','ikom','im1k','inno','ipaq','iris',
        // J
        'jata','java','jbro','jemu','jigs',
        // K
        'kddi','keji','kgt' ,'kgt/','klon','kpt ','kwc-','kyoc',
        'kyok',
        // L
        'leno','lexi','lg g','lg-a','lg-b','lg-c','lg-d','lg-f',
        'lg-g','lg-k','lg-l','lg-m','lg-o','lg-p','lg-s','lg-t',
        'lg-u','lg-w','lg/k','lg/l','lg/u','lg50','lg54','lge-',
        'lge/','libw','lynx',
        // M
        'm-cr','m1-w','m3ga','m50/','mate','maui','maxo','mc01',
        'mc21','mcca','medi','merc','meri','mio8','mioa','mits',
        'mmef','mo01','mo02','mode','modo','mot ','mot-','moto',
        'motv','mozz','mt50','mtp1','mtv ','mwbp','mywa',
        // N
        'n100','n101','n102','n202','n203','n300','n302','n500',
        'n502','n505','n700','n701','n710','nec-','nem-','neon',
        'netf','newg','newt','nok6','noki','nzph',
        // O
        'opti','opwv','oran','owg1',
        // P
        'p800','palm','pana','pand','pant','pdxg','pg-1','pg-2',
        'pg-3','pg-6','pg-8','pg-c','pg13','phil','pire','play',
        'pluc','port','pn-2','pose','ppc;','prox','psio','pt-g',
        // Q
        'qa-a','qc-2','qc-3','qc-5','qc-7','qc07','qc12','qc21',
        'qc32','qc60','qci-','qtek',
        // R
        'r380','r600','raks','rim9','rove','rozo',
        // S
        's55/','sage','sama','samm','sams','sany','sava','sc01',
        'sch-','scp-','sdk/','se47','sec-','sec0','sec1','semc',
        'send','seri','sgh-','shar','sie-','sk-0','sl45','slid',
        'smal','smar','smb3','smt5','scoo','sp01','sph-','spv ',
        'spv-','sy01','siem','smit','soft','sony','symb',
        // T
        't-mo','t218','t250','t600','t610','t618','tagt','talk',
        'tcl-','tdg-','teli','telm','tim-','ts70','tsm-','tsm3',
        'tsm5','tx-9','topl','tosh',
        // U
        'upg1','upsi','utst',
        // V
        'v400','v750','veri','virg','vite','vk-v','vk40','vk50',
        'vk52','vk53','vm40','voda','vulc','vx51','vx53','vx60',
        'vx61','vx70','vx80','vx81','vx83','vx85','vx98',
        // W
        'webc','whit','wig ','winc','winw','wmlb','wonu',
        // X
        'x700','xda-','xda2','xdag',
        // Y
        'yas-','your',
        // Z
        'zte-','zeto',
      ),
    ),
  );

  // we are not using Unicode functions since the AGENT string
  // is expected to be ASCII
  $agent = strtolower($agent);
  $sub_agent = substr($agent, 0, 4);
  foreach ($mobiles as $device => $info) {
    if (isset($info['include'])) {
      if (strpos($agent, $info['include']) !== FALSE) {
        return $device;
      }
    }
    if (isset($info['match'])) {
      if (preg_match($info['match'], $agent)) {
        return $device;
      }
    }
    if (isset($info['start'])) {
      if (in_array($agent, $info['start'])) {
        return $device;
      }
    }
  }

  // if not a mobile phone, it's a desktop computer
  return 'desktop';
}

// vim: ts=2 sw=2 et syntax=php
