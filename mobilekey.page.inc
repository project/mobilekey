<?php

/** @brief Determine whether the browser runs on a mobile device.
 *
 * This function checks whether the user is forcing the look and
 * feel of the output to be based on mobile or desktop device.
 *
 * The function checks for a URL variable named 'device' which is
 * set to a value (other than empty string).
 *
 * The name 'device' is also used by mobile_tools in a very similar
 * manner. At this time, the values should be compatible. However,
 * it may slowly start to diverge.
 *
 * @return The mobile group name.
 */
function mobilekey_get_device() {
  global $device, $cookie_domain;

  // the result is cached to avoid defining it more than once
  if (!empty($device)) {
    return $device;
  }

  // user forces the device in the URL?
  // (this is useful when testing or for including links between modes)
  if (!empty($_GET['device'])) {
    // determine the duration of the session (default to 8 days = 86400 * 8)
    $device = $_GET['device'];
  }
  elseif (!empty($_COOKIE['mk_device'])) {
    // get the device from the cookie
    $device = $_COOKIE['mk_device'];
  }
  elseif (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])) {
    // only mobiles have profiles
    $device = 'mobile';
  }
  elseif (!empty($_SERVER['HTTP_ACCEPT'])
      && (strpos($_SERVER['HTTP_ACCEPT'], 'text/vnd.wap.wml') !== FALSE
       || strpos($_SERVER['HTTP_ACCEPT'], 'application/vnd.wap.xhtml+xml') !== FALSE)) {
    // if ACCEPT includes 'wap' it is likely a mobile device
    $device = 'mobile';
  }
  elseif (!empty($_SERVER['HTTP_USER_AGENT'])) {
    // use the USER AGENT variable to determine the device
    // check the user agent, this is a biggie so we have it in a separate
    // include file; and the cookie caches the result
    // note: this is the slowest so we do it last
    module_load_include('device.inc', 'mobilekey');
    $device = mobilekey_user_agent_to_device($_SERVER['HTTP_USER_AGENT']);
  }
  else {
    // default is assumed to be desktop
    $device = 'desktop';
  }

  // reset the cookie so the session is extended
  $session_time = time() + variable_get('mobilekey_cookie_session', 691200);
  setCookie('mk_device', $device, $session_time, '/', $cookie_domain);

  // return the result
  return $device;
}

// vim: ts=2 sw=2 et syntax=php
