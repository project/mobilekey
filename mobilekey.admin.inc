<?php

/** @brief Actual implementation of the mobilekey_menu() function.
 *
 * Moved here to optimize the module (menu are cached and the definitions
 * are only rarely necessary.)
 *
 * @return An array of menu items.
 */
function mobilekey_menu_array() {
  $items = array();

  $items['admin/settings/mobilekey'] = array(
    'title' => 'Mobile key',
    'description' => 'Edit mobile key settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mobilekey_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'mobilekey.admin.inc'
  );

  return $items;
}

/** @brief Implementation of settings menu entry.
 *
 * This function handles the global settings of the mobilekey module.
 *
 * @return An array representing the administration form.
 */
function mobilekey_admin_settings() {
  $form = array();

  $form['redirect'] = array(
    '#type' => 'fieldset',
    '#title' => t('Redirection'),
  );
  $form['redirect']['mobilekey_mobile_front_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Mobile device front page URL'),
    '#default_value' => variable_get('mobilekey_mobile_front_page', ''),
    '#description' => t('By default, the module does not redirect your users. When you enter a URL to a different page here, users on mobile devices that go to your home page will be redirected to this page instead. The URL can be an absolute path or a local path.'),
  );

  return system_settings_form($form);
}

// vim: ts=2 sw=2 et syntax=php
